/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package com.badlogicgames.superjumper;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Cursor;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.utils.Timer;

import java.sql.Time;

public class Bob extends DynamicGameObject {
	public static final int BOB_STATE_JUMP = 0;
	public static final int BOB_STATE_FALL = 1;
	public static final int BOB_STATE_HIT = 2;
	public static final int BOB_STATE_FLY = 3;
	public static final float BOB_JUMP_VELOCITY = 11;
	public static final float BOB_MOVE_VELOCITY = 20;
	public static final float BOB_WIDTH = 0.8f;
	public static final float BOB_HEIGHT = 0.8f;
	public int BOB_LIVES = 3;

	int state;
	float stateTime;
	Timer stuntTime;
	Timer flyTime;
	Timer.Task release;
	boolean haveJetpack;
	float jetpackV;
	Sprite jetpackOn;

	public Bob (float x, float y) {
		super(x, y, BOB_WIDTH, BOB_HEIGHT);
		state = BOB_STATE_FALL;
		stateTime = 0;
		haveJetpack = false;
		jetpackV = 10;
		jetpackOn = new Sprite(Assets.jetpack);
	}

	public void update (float deltaTime) {
		velocity.add(World.gravity.x * deltaTime, World.gravity.y * deltaTime);
		position.add(velocity.x * deltaTime, velocity.y * deltaTime);
		bounds.x = position.x - bounds.width / 2;
		bounds.y = position.y - bounds.height / 2;

		if (velocity.y > 0 && state != BOB_STATE_HIT) {
			if(state != BOB_STATE_FLY) {
				if (state != BOB_STATE_JUMP) {
					state = BOB_STATE_JUMP;
					stateTime = 0;
				}
			}
		}

		if (velocity.y < 0 && state != BOB_STATE_HIT ) {
			if(state != BOB_STATE_FLY)
			{
				if (state != BOB_STATE_FALL) {
					state = BOB_STATE_FALL;
					stateTime = 0;
				}
			}
		}

		if(haveJetpack && state != BOB_STATE_HIT)
		{
			if(state != BOB_STATE_FLY){
				state = BOB_STATE_FLY;
			}
		}

		if (position.x < 0) position.x = World.WORLD_WIDTH;
		if (position.x > World.WORLD_WIDTH) position.x = 0;
		if(haveJetpack){
			stateTime = 0;
			if(state != BOB_STATE_HIT)velocity.y = jetpackV;
			jetpackOn.setPosition(position.x, position.y);
		}

		stateTime += deltaTime;
	}

	public void hitSquirrel () {
		BOB_LIVES--;
		state = BOB_STATE_HIT;
		Assets.bloodEffect.setPosition(position.x, position.y);
		if(!Assets.bloodEffect.isComplete())
		{
			Assets.bloodEffect.reset();
		}else Assets.bloodEffect.start();
		if(BOB_LIVES == 0) {
			velocity.set(0, 0);
			stateTime = 0;
		}else{
			stunt();
		}
	}

	private void stunt()
	{
		stuntTime = new Timer();
		stuntTime.scheduleTask(release = new Timer.Task() {
			@Override
			public void run() {
				if(velocity.y < 0)
				{
					state = BOB_STATE_FALL;
				}else state = BOB_STATE_JUMP;
			}
		}, 0.2f);
	}

	public void hitPlatform () {
		velocity.y = BOB_JUMP_VELOCITY;
		state = BOB_STATE_JUMP;
		stateTime = 0;

		Assets.dustEffect.setPosition(position.x, position.y-BOB_HEIGHT/2);
		Assets.dustEffect.start();
	}

	public void hitSpring () {
		velocity.y = BOB_JUMP_VELOCITY * 1.5f;
		state = BOB_STATE_JUMP;
		stateTime = 0;
	}

	public void hitJetpack () {
		haveJetpack = true;
		state = BOB_STATE_FLY;
		endJetpack();
	}

	private void endJetpack()
	{
		flyTime = new Timer();
		flyTime.scheduleTask(release = new Timer.Task() {
			@Override
			public void run() {
				haveJetpack = false;
			}
		}, 2f);
	}
}
