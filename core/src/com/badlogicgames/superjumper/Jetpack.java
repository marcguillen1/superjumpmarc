package com.badlogicgames.superjumper;

import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * Created by CASA on 15/04/2017.
 */

public class Jetpack extends GameObject {
    public static final float JETPACK_WIDTH = 0.9f;
    public static final float JETPACK_HEIGHT = 0.9f;
    private Sprite jetpack;

    float stateTime;

    public Jetpack (float x, float y) {
        super(x, y, JETPACK_WIDTH, JETPACK_HEIGHT);
        stateTime = 0;
        jetpack = new Sprite(Assets.jetpack);
        jetpack.setSize(16,16);
    }

    public void update (float deltaTime) {
        stateTime += deltaTime;
    }
}
